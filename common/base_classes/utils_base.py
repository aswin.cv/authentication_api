class UtilsBase:

    @staticmethod
    def validate_data(data, serializer_cls):
        serializer_cls(data=data).is_valid(raise_exception=True)
        return data
