from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.views import exception_handler


def api_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None:
        response.data['status_code'] = response.status_code

        response.data['error']: response.error
        # replace detail key with message key by delete detail key
        response.data['msg'] = response.data['detail']
        del response.data['detail']

    return response


class DefaultException(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    detail = 'A server error occurred.'
    error = 'error'


class AuthException(DefaultException):
    detail = None
    status_code = None
    error = None

    # create constructor
    def __init__(self, message, error):
        # override public fields
        AuthException.status_code = 503
        AuthException.detail = message
        AuthException.error = error


class KeyException(DefaultException):
    detail = None
    status_code = None
    error = None

    # create constructor
    def __init__(self, message):
        # override public fields
        KeyException.status_code = 500
        KeyException.detail = message
        KeyException.error = "KeyError"


class DAOException(DefaultException):
    detail = None
    status_code = None
    error = None

    # create constructor
    def __init__(self, message):
        # override public fields
        DAOException.status_code = 500
        DAOException.detail = message
        DAOException.error = "DAO processing error"


class ServiceException(DefaultException):
    detail = None
    status_code = None
    error = None

    # create constructor
    def __init__(self, message):
        # override public fields
        DAOException.status_code = 500
        DAOException.detail = message
        DAOException.error = "Service processing error"
