INVALID_CREDENTIALS = 'Invalid credentials'
USER_DOES_NOT_EXIST = 'User does not exist or is inactive'
INVALID_DATA = 'Invalid data'
GENERIC_ERROR = 'Oops! Something went wrong'
