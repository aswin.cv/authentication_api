from rest_framework import serializers


class ToeknAuthSignOutSerializer(serializers.Serializer):
    refresh_token = serializers.CharField(required=True)
    access_token = serializers.CharField(required=False)