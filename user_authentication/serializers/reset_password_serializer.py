from rest_framework import serializers


class ResetPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(required=True)
