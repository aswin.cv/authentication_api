from common.base_classes.utils_base import UtilsBase


class RegistrationUtils(UtilsBase):

    @staticmethod
    def generate_response_from_user_object(user_obj):
        return {
            "id": user_obj.uid,
            "email": user_obj.email
        }
