import json

from django.urls import reverse
from rest_framework.test import APITestCase, APIClient


class UserRegistrationViewTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient(HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.email = 'testuser@gmail.com'
        self.first_name = 'John'
        self.last_name = 'Doe'
        self.password = "test@123"

    def make_request(self, data):
        return self.client.post(reverse('user_registration'), json.dumps(data), content_type='application/json')

    def test_missing_email(self):
        response = self.make_request(
            {'first_name': self.first_name, 'last_name': self.last_name, 'password': self.password})
        self.assertEqual(response.status_code, 400)

    def test_missing_name(self):
        response = self.make_request({'email': self.email})
        self.assertEqual(response.status_code, 400)

    def test_empty_email(self):
        response = self.make_request(
            {'email': '', 'first_name': self.first_name, 'last_name': self.last_name, 'password': self.password})
        self.assertEqual(response.status_code, 400)

    def test_invalid_email(self):
        response = self.make_request({'email': 'random@string', 'first_name': self.first_name,
                                      'last_name': self.last_name, 'password': self.password})
        self.assertEqual(response.status_code, 400)

    def make_valid_request(self):
        return self.make_request({'email': self.email, 'first_name': self.first_name,
                                  'last_name': self.last_name, 'password': self.password})

    def test_sign_up(self):
        response = self.make_valid_request()
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['email'], self.email)
