from unittest import TestCase

from common.exceptions import DAOException
from user_authentication.daos.user_dao import UserDAO
from user_authentication.models import User


class DAOSTestBase(TestCase):
    pass


class UserDAOTest(DAOSTestBase):
    dao = UserDAO()

    def setUp(self):
        User.objects.all().delete()

    def tearDown(self):
        User.objects.all().delete()

    def test_create_user_with_valid_data(self):
        validated_data = {"email": "testuser@gmail.com", "first_name": "test", "last_name": "user",
                          "password": "test@123"}
        self.dao.create_user(validated_data)
        expected_result = 1
        user_objects_count = User.objects.count()
        self.assertEquals(expected_result, user_objects_count)
        User.objects.all().delete()

    # def test_create_user_with_same_email_multiple_times(self):
    #     validated_data = {"first_name": "test", "last_name": "user",
    #                       "password": "test@123"}
    #     obj = self.dao.create_user(validated_data)
    #     with self.assertRaises(DAOException):
    #         self.dao.create_user(validated_data)
