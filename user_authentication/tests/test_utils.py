from unittest import TestCase

from rest_framework import serializers

from user_authentication.daos.user_dao import UserDAO
from user_authentication.serializers import registration_serializer
from user_authentication.utils.registration_utils import RegistrationUtils


class TestUtilsBase(TestCase):
    pass


class TestRegistrationUtils(TestUtilsBase):
    utils_cls = RegistrationUtils

    def test_validate_data_with_valid_data(self):
        valid_data = {"email": "testuser@gmail.com", "first_name": "test", "last_name": "user",
                      "password": "test@123"}
        ret_data = self.utils_cls.validate_data(valid_data, registration_serializer.CreteUserSerializer)
        self.assertDictEqual(valid_data, ret_data)

    def test_validate_data_with_invalid_data(self):
        invalid_data = {"first_name": "test", "last_name": "user",
                        "password": "test@123"}
        with self.assertRaises(serializers.ValidationError):
            self.utils_cls.validate_data(invalid_data, registration_serializer.CreteUserSerializer)

    def test_generate_response_from_user_object(self):
        validated_data = {"email": "testuser@gmail.com", "first_name": "test", "last_name": "user",
                          "password": "test@123"}
        user_obj = UserDAO().create_user(validated_data)
        expected_result = {
            "id": user_obj.uid,
            "email": user_obj.email
        }
        result = self.utils_cls.generate_response_from_user_object(user_obj)
        self.assertDictEqual(expected_result, result)
