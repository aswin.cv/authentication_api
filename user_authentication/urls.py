from django.conf import settings
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views

from user_authentication.views import registration_views
from user_authentication.views import reset_password_views
from user_authentication.views import signin_views
from user_authentication.views import signout_views
from user_authentication.views import test_views
from user_authentication.views import token_black_list_views

token_auth_url_patterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('token/black-list/', token_black_list_views.BlackListTokenAuthView.as_view(), name="black_list_token")
]

session_auth_url_patterns = [
    path('sign-in/', signin_views.SignInView.as_view(), name='sing_in'),
    path('sign-out/', signout_views.SignOutView.as_view(), name='sing_out'),
    path('test-endpoint/', test_views.TestAuthView.as_view(), name="test_auth_endpoint"),
]

urlpatterns = [
    path('register/', registration_views.UserRegistration.as_view(), name='user_registration'),
    path('reset-password/', reset_password_views.ResetPassword.as_view(), name="reset_password"),
]

if settings.ENABLE_SESSION_AUTH:
    urlpatterns.append(path('', include((session_auth_url_patterns, 'session_auth'),
                                        namespace='session_auth')))
if settings.ENABLE_TOKEN_AUTH:
    urlpatterns.append(path('', include((token_auth_url_patterns, 'token_auth'),
                                        namespace='token_auth')))
