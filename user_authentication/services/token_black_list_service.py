from rest_framework_simplejwt.tokens import RefreshToken


class BlackListService:
    @staticmethod
    def black_list_token(token):
        try:
            token = RefreshToken(token)
            token.blacklist()
        except Exception as e:
            print(e)
            ServiceException("Failed to Black List Token")
