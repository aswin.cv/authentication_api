from django.contrib.auth import logout, login, update_session_auth_hash

from common.exceptions import ServiceException
from user_authentication.daos.user_dao import UserDAO


class ResetPasswordService:
    user_dao = UserDAO()

    @staticmethod
    def update_session(request):
        try:
            update_session_auth_hash(request, request.user)
        except Exception as e:
            print(e)
            raise ServiceException("Failed to reset session")

    def reset_password(self, request, validated_data):
        self.user_dao.reset_password(request, validated_data)
        self.update_session(request)
        return {"status": "success"}
