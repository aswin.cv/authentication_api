from user_authentication.daos.user_dao import UserDAO


class RegistrationService:
    user_dao = UserDAO()

    def create_new_user(self, validated_data):
        return self.user_dao.create_user(validated_data)
