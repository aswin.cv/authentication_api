import requests
from django.conf import settings
from django.contrib.auth import logout
from django.urls import reverse
from rest_framework_simplejwt.tokens import RefreshToken

from common.exceptions import ServiceException


class SignOutService:
    def sign_out_user(self, request):
        try:
            logout(request)
        except Exception as e:
            print(e)
            ServiceException("Failed to Black List Token")
        return {"status": "success"}
