import requests
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.urls import reverse

from common.exceptions import ServiceException


class SignInService:

    def fetch_token_jwt_token_pair(self, request, validated_data):
        token_obtain_url = request.build_absolute_uri(reverse('token_auth:token_obtain_pair'))
        return requests.post(token_obtain_url, data=dict(validated_data)).json()

    def authenticate_user(self, request, validated_data):
        user = authenticate(email=validated_data["email"], password=validated_data["password"])
        if user is not None:
            login(request, user)
            if settings.ENABLE_TOKEN_AUTH:
                return self.fetch_token_jwt_token_pair(request, validated_data)
            return {"status": "success"}
        else:
            raise ServiceException("Failed to SignIn")
