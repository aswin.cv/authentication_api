from common.exceptions import DAOException
from user_authentication.models import User


class UserDAO:
    def create_user(self, validated_data):
        try:
            validated_data = dict(validated_data)
            return User.objects.create(**validated_data)
        except Exception as e:
            print(e)
            DAOException("Failed to create a new user")

    def reset_password(self, request, validated_data):
        try:
            user = request.user
            user.set_password(validated_data["password"])
            user.save()
        except Exception as e:
            print(e)
            DAOException("Failed to update password")
