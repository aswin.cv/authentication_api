from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView

from common import constants as common_constants
from user_authentication.serializers import signin_serializer
from user_authentication.services.signin_service import SignInService
from user_authentication.utils.signin_utils import SignInUtils


class SignInView(APIView):
    service_obj = SignInService()
    utils_cls = SignInUtils()
    permission_classes = ()

    def post(self, request, **kwargs):
        response_data = {}
        try:
            validated_data = self.utils_cls.validate_data(request.data, signin_serializer.SignInSerializer)
            response_data = self.service_obj.authenticate_user(request, validated_data)
            return Response(response_data, status=status.HTTP_201_CREATED)
        except serializers.ValidationError as error:
            response_data['msg'] = common_constants.INVALID_DATA
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            response_data['msg'] = common_constants.GENERIC_ERROR
        return Response(response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
