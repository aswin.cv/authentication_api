from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from common import constants as common_constants
from user_authentication.services.signout_service import SignOutService
from user_authentication.utils.signout_utils import SignOutUtils


class SignOutView(APIView):
    service_obj = SignOutService()
    utils_cls = SignOutUtils

    def post(self, request, **kwargs):
        response_data = {}
        try:
            response_data = self.service_obj.sign_out_user(request)
        except Exception as e:
            print(e)
            response_data['msg'] = common_constants.GENERIC_ERROR
        return Response(response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
