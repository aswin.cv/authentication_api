from rest_framework import status, serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from common import constants as common_constants
from user_authentication.serializers import reset_password_serializer
from user_authentication.services.reset_password_service import ResetPasswordService
from user_authentication.utils.reset_password_utils import ResetPasswordUtils


class ResetPassword(APIView):
    service_obj = ResetPasswordService()
    utils_cls = ResetPasswordUtils

    def post(self, request, **kwargs):
        response_data = {}
        try:
            print(request.user)
            validated_data = self.utils_cls.validate_data(request.data,
                                                          reset_password_serializer.ResetPasswordSerializer)
            response_data = self.service_obj.reset_password(request, validated_data)
            return Response(response_data, status=status.HTTP_201_CREATED)
        except serializers.ValidationError as error:
            response_data['msg'] = common_constants.INVALID_DATA
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            response_data['msg'] = common_constants.GENERIC_ERROR
        return Response(response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
