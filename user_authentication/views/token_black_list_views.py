from rest_framework import status, serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from common import constants as common_constants
from user_authentication.services.token_black_list_service import BlackListService
from user_authentication.utils.token_black_list_utils import TokenBlackListUtils
from user_authentication.serializers import token_black_list_serializer


class BlackListTokenAuthView(APIView):
    service_obj = BlackListService()
    utils_cls = TokenBlackListUtils

    def post(self, request):
        response_data = {}
        try:
            validated_data = self.utils_cls.validate_data(data=request.data,
                                                          serializer_cls=
                                                          token_black_list_serializer.ToeknAuthSignOutSerializer)
            # self.service_obj.black_list_token(validated_data['access_token'])
            self.service_obj.black_list_token(validated_data['refresh_token'])
            response_data = {"status": "success"}
            return Response(response_data, status=status.HTTP_205_RESET_CONTENT)
        except serializers.ValidationError as error:
            response_data['msg'] = common_constants.INVALID_DATA
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            response_data['msg'] = common_constants.GENERIC_ERROR
        return Response(response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
