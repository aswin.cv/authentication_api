from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView

from common import constants as common_constants
from user_authentication.serializers import registration_serializer
from user_authentication.services.registration_service import RegistrationService
from user_authentication.utils.registration_utils import RegistrationUtils


class UserRegistration(APIView):
    permission_classes = ()
    service_obj = RegistrationService()
    utils_cls = RegistrationUtils

    def post(self, request):
        response_data = {}
        try:
            validated_data = self.utils_cls.validate_data(request.data, registration_serializer.CreteUserSerializer)
            user_obj = self.service_obj.create_new_user(validated_data)
            response_data = self.utils_cls.generate_response_from_user_object(user_obj)
            return Response(response_data, status=status.HTTP_201_CREATED)
        except serializers.ValidationError as error:
            response_data['msg'] = common_constants.INVALID_DATA
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(e)
            response_data['msg'] = common_constants.GENERIC_ERROR
        return Response(response_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
