# README

## Authentication API

Authentication is the process of verifying the identity of an individual. A user can interact with a web application
using multiple actions. Access to certain actions or pages can be restricted using user levels. This project is aimed to
implement endpoints that are required for session authentication , token authentication or combination of both.

this project is implemented using `python3.8`

### Setup

The first thing to do is to clone the repository:

```shell
git clone https://gitlab.com/aswin.cv/authentication_api
cd sample-django-app
```

Create a virtual environment to install dependencies in and activate it:

```shell
virtualenv --no-site-packages env
source env/bin/activate
```

Then install the dependencies:

```shell
pip install -r requirements.txt
```

Note the (env) in front of the prompt. This indicates that this terminal session operates in a virtual environment set
up by virtualenv.

Once pip has finished downloading the dependencies:

```shell
python manage.py runserver
```

And navigate to http://127.0.0.1:8000/

it's possible to customise token auth and session auth using following parmas

- ENABLE_TOKEN_AUTH
- ENABLE_SESSION_AUTH

### Available EndPoints

common for both session and token auth

- register/
- reset-password/
- test-endpoint/

specific to session auth

- sign-in/
- sign-out/

specific to token auth

- token/
- token/refresh/
- token/black-list/